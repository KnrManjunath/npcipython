1 # Python has functions for creating, reading, 2 2 2.      updating, and deleting files.
2
3 # Open a file
4 myFile = open('myfile.txt', 'w')
5
6 # Get some info
7 print('Name: ', myFile.name)
8 print('Is Closed : ', myFile.closed)
9 print('Opening Mode: ', myFile.mode)
10
11 # Write to file
12 myFile.write('I love Python')
13 myFile.write(' and JavaScript')
14 myFile.close()
15
16 # Append to file
17 myFile = open('myfile.txt', 'a')
18 myFile.write(' I also like PHP')
19 myFile.close()
20
21 # Read from file
22 myFile = open('myfile.txt', 'r+')
23 text = myFile.read(100)
24 print(text)
